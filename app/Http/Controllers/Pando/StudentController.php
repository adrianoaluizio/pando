<?php

namespace App\Http\Controllers\Pando;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pando\Student;
use App\Pando\Course;
use App\Pando\CourseStudent;
use PDF;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $students = Student::all();
      return view('student.list', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $courses = Course::all();
      return view('student.register',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $student = Student::create($request->all());
      foreach ($request->course as $course) {
        CourseStudent::create([
            'student_id' => $student->id,
            'course_id' => $course
        ]);
    }
      return redirect()->route('aluno.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $student = Student::find($id);
      return view('student.details', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $student = Student::find($id);
      $courses = Course::all();
      return view('student.alter', compact('student','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $student = Student::find($id);
      $student->update($request->all());
      foreach ($request->course as $course) {
        CourseStudent::updateOrCreate([
          'student_id' => $id,
          'course_id' => $course
        ]);
      }
      return view('student.details', compact('student'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $student = Student::find($id)->delete();;
      return redirect()->route('aluno.index');
    }


    public function pdf(Request $request){
        $student = Student::find($request->student);
        $course = Course::find($request->course);

		$pdf = PDF::loadView('certified.certified',compact('student','course'));
		$pdf->setPaper('a4','landscape');
		return $pdf->stream();
        // return view("certified.certified");
    }
}
