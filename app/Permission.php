<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);
    }

    public function ListType($type)
    {
        $permission = $this::where('type',$type)->get();

        return $permission;
    }

    public function verification($perm)
    {
        if($this->id == $perm){
            $result = 'checked';
        }else{
            $result = false;
        }
        return $result;
    }
}
