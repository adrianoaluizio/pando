<?php

namespace App\Pando;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name','time'];


    public function verification($perm)
    {
        if($this->id == $perm){
            $result = 'checked';
        }else{
            $result = false;
        }
        return $result;
    }
}
