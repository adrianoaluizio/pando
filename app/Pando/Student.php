<?php

namespace App\Pando;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $fillable = ['name'];

  public function courses()
  {
      return $this->belongsToMany(\App\Pando\Course::class);
  }
}
