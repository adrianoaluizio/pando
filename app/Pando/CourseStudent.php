<?php

namespace App\Pando;

use Illuminate\Database\Eloquent\Model;

class CourseStudent extends Model
{
	protected $table = 'course_student';
    protected $fillable = ['student_id', 'course_id'];
}
