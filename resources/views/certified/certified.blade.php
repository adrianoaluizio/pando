<html>
    <head>
        <style>
        body{
        background-image: url('images/certificado.jpg');
        background-repeat: no-repeat;
        background-size: 100%, 100%;
        background-attachment: fixed;
        background-position: center;
        }
        .paper {
        padding: 0;
        background: none;
        display: block;
        width: 100%;
        height:14cm;
        font-family: 'Roboto', sans-serif;
        }
        .paper h1{
        font-family: 'Roboto', sans-serif;
        margin-top: 100px;
        font-size: 3em;
        font-weight: bold;
        text-align: center;
        }
        .paper h2{
        font-family: 'Roboto', sans-serif;
        font-size: 0.9em;
        font-weight: bold;
        text-align: center;
        padding-top: 1.5em;
        }
        .paper div{
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
        }
        .paper p{
        margin: 0;
        padding: 50px 150px 0 150px;;
        border: 0;
        font-size: 2em;
        font: inherit;
        line-height: 1.5em;
        vertical-align: baseline;
        }


        }
        </style>
        <title>Certificado de Conclusão</title>
    </head>
    <body>
        <div class="paper">
            <h1>Certificado de Conclusão</h1>
            <p>
                Certificamos para os devidos fins que {{$student->name}}, concluiu, curso de extensão: {{$course->name}} junto ao
                Departamento de Tecnologia da PANDO, com carga horária de {{$course->time}}.
            </p>
            <p>Data: {{date('d/m/Y')}}</p>
        </div>
    </body>
</html>
