<!-- Modal -->
<div class="modal fade" id="excluir{{$id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h1>{{$text}}</h1>
      </div>
      <div class="modal-footer">
        <a class="btn btn-dash" data-dismiss="modal">Fechar</a>
        <form action="{{$link}}" method="post">
          @csrf
          @method('DELETE')
          <button class="btn btn-dash" type="submit">Excluir</button>
        </form>
      </div>
    </div>
  </div>
</div>