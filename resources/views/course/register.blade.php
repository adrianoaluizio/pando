@extends('layouts.theme')
@section('content')
{{Breadcrumbs::render('student')}}
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form method="POST" action="{{route ('curso.store')}}">
        @csrf
        <div class="card">
          <div class="card-header">
            <h1 class="title">{{__('Cadastro de Curso')}}</h1>
          </div>
          <div class="card-body">
            <div class="form-row" id="form">
              <div class="form-group col-md-8">
                {!!Form::label('name', 'Nome:')!!}
                {!!Form::text('name', null,['class'=>'form-control'])!!}
              </div>
              <div class="form-group col-md-4">
                {!!Form::label('time', 'Carga horária:')!!}
                {!!Form::text('time', null,['class'=>'form-control'])!!}
              </div>
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-dash float-right">Cadastrar</button>
            <a href="{{ route('curso.index') }}" class="btn btn-dash float-left">
              <i class="fas fa-arrow-left"></i>
            </a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
