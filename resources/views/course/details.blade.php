@extends ('layouts.theme')
@section('content')
{{Breadcrumbs::render('course-show',$course)}}
<div class="container-fluid">
  {{-- Details --}}
  <div class="row">
    <div class="col-md-12">
      <div class="card mb-3">
        <div class="card-header">
          <div class="row">
          <div class="col-md-8">
            <h1 class="title"><i class="fas fa-users"></i> {{$course->name}}</h1>
          </div>
          <div class="col-md-4">
            <a class="btn-del float-right" data-toggle="modal" data-target="#excluir{{$course->id}}" href="#" title="Excluir">
                <i class="fas fa-trash"></i>
              </a>
            <a class="btn-edit float-right mr-1" href="{{ route('curso.edit',$course->id) }}" title="Alterar">
              <i class="fas fa-edit"></i>
            </a>
            <a href="{{ route('curso.index') }}" class="btn-dash float-right mr-1" title="Voltar">
              <i class="fas fa-arrow-left"></i>
            </a>
          </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">

            <div class="col-md-8">
              <div class="line-chart">
                <small>Nome</small>
                <p>{{$course->name }}</p>
              </div>
            </div>
            <div class="col-md-4">
                <div class="line-chart">
                  <small>Carga Horária</small>
                  <p>{{$course->time }}</p>
                </div>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  {{-- End Details --}}


  {{-- Card Services --}}

</div>
@component('extends.modal')
  @slot('link')
    {{ route('curso.destroy',$course->id) }}
  @endslot
  @slot('text')
    Tem certeza que deseja excluir ?
  @endslot
  @slot('id')
    {{$course->id}}
  @endslot
@endcomponent

@endsection
