@extends('layouts.theme')
@section('content')
{{Breadcrumbs::render('course-edit',$course)}}
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form method="POST" action="{{route ('curso.update',$course->id)}}">
        @csrf
        @method('PATCH')
        <div class="card">
          <div class="card-header">
            <h1 class="title">{{__('Alterar Curso')}}</h1>
          </div>
          <div class="card-body">
            <div class="form-row" id="form">
              <div class="form-group col-md-8">
                {!!Form::label('name', 'Nome:')!!}
                {!!Form::text('name', $course->name,['class'=>'form-control'])!!}
              </div>
              <div class="form-group col-md-4">
                {!!Form::label('time', 'Carga Horária:')!!}
                {!!Form::text('time', $course->time,['class'=>'form-control'])!!}
              </div>

            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-dash float-right">Alterar</button>
            <a href="{{ route('curso.index') }}" class="btn btn-dash float-left">
              <i class="fas fa-arrow-left"></i>
            </a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
