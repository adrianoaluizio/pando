<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('images/favicon.png') }}">
    <title>PANDO&#45; Impressão de certificado</title>
    <!-- Plugins CSS -->
    <link href="{{asset ('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Custom style -->
    <link href="{{asset ('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset ('css/select2.min.css')}}">
    <link href="{{asset ('css/datatables.min.css')}}" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="{{asset ('css/roboto.css')}}" rel="stylesheet">
    <link href="{{ asset('css/libre.css') }}" rel="stylesheet">
    <link href="{{ asset('css/quicksand.css') }}" rel="stylesheet">

    @php
    foreach(Auth::user()->roles as $role ){
      $role_id = $role->id;
    }
    @endphp

    <script>
      window.Laravel = {!! json_encode([
        'csrf' => csrf_token(),
        'pusher' => [
          'key' => config('broadcasting.connections.pusher.key'),
          'cluster' => config('broadcasting.connections.pusher.options.cluster'),
        ],
        'role' => $role_id,
      ]) !!}
    </script>
  </head>
  {{-- Body Theme --}}
  <body>
    <div id="app" class="wrapper">
      <!-- Sidebar  -->
      <nav id="sidebar">
        {{-- Logo --}}
        <div class="sidebar-header">
          <div class="logo">
            <img src="{{asset ('images/logo-branco.png')}}" class="img-fluid" alt="">
          </div>
          <div class="small-logo">
            <img src="{{asset ('images/logo-min.png')}}" alt="">
          </div>
        </div>
        {{-- Menu --}}

        <ul class="list-unstyled components">
          <li>
            <a href="{{ route('dashboard') }}">
              <i class="fas fa-tachometer-alt"></i>
              <span>Painel de Controle</span>
            </a>
          </li>
          <li>
            <a href="{{ route('aluno.index') }}">
              <i class="fas fa-user-edit"></i>
              <span>Aluno</span>
            </a>
          </li>
          <li>
            <a href="{{ route('curso.index') }}">
                <i class="fas fa-book-open"></i>
              <span>Cursos</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- Page Content  -->
      <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-menu">
            <i class="fas fa-align-left"></i>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown dropleft">
                  <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bem-vindo,
                    <span>{{ Auth::user()->name }}</span>
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{route ('logout')}}"><i class="fas fa-sign-out-alt"></i> Sair</a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </ul>
            </div>
          </div>
        </nav>

        @yield('content')

      </div>
    </div>
  <footer></footer>
  <script src="{{asset ('js/app.js')}}"></script>
  {{-- <script src="{{asset ('bootstrap/js/bootstrap.min.js')}}"></script> --}}
  <script src="{{asset ('popper/popper.min.js')}}"></script>
  <script src="{{asset ('js/datatables.min.js')}}"></script>
  <script src="{{asset ('js/json/pt-br.json')}}"></script>
  <script src="{{asset ('js/select2.full.min.js')}}"></script>
  {{-- END TREE VIEW --}}
  <script src="{{asset ('js/moment.min.js')}}"></script>
  <script src="{{asset ('js/mask.js')}}"></script>
  <script src="{{asset ('js/main.js')}}"></script>


  @stack('script')
</body>
</html>
