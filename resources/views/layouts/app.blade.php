<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="{{asset ('images/favicon.png')}}">
  <title>PANDO&#45; Impressão de certificado</title>

  <link href="{{asset ('css/app.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- Custom style -->
  <link href="{{asset ('css/style.css')}}" rel="stylesheet">

  <!-- Google Fonts -->
  <link href="{{ asset('css/roboto.css') }}"rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        @yield('content')
    </div>
</body>
<script src="{{asset ('js/app.js')}}"></script>

</html>
