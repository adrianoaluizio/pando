@extends('layouts.app') @section('content')
<div class="container">
  <div class="row align-items-center">
    <div class="col-lg-4 offset-lg-4">
      <div class="m-100">
        {{-- Login --}}
        <div class="box-login">
          <div class="logo mb-3">
            <img src="{{asset ('images/logo.png')}}" class="img-fluid" alt="Logo Multierri">
          </div>
          <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                  <i class="far fa-envelope"></i>
                </span>
              </div>
              <input id="email" type="email" placeholder="E-mail:" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
              name="email" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
              <div class="invalid-feedback">
                {{ $errors->first('email') }}
              </div>
              @endif
            </div>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                  <i class="fas fa-key"></i>
                </span>
              </div>
              <input id="password" type="password" placeholder="Senha:" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
              name="password" required> @if ($errors->has('password'))
              <div class="invalid-feedback">
                {{ $errors->first('password') }}
              </div>
              @endif
            </div>
            <div class="form-group">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{
                old( 'remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                  {{ __('Lembrar-me') }}
                </label>
                <a href="{{ route('password.request') }}">
                  {{ __('Esqueceu sua senha ?') }}
                </a>
              </div>
            </div>
            <button type="submit" class="btn btn-login">
            {{ __('Entrar') }}
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
