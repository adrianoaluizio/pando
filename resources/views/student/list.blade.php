@extends ('layouts.theme')
@section('content')
{{Breadcrumbs::render('student')}}
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        {{-- Header Card --}}
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              <h1 class="title">{{ __('Lista de Alunos') }}</h1>
            </div>
            <div class="col-md-4">
              <a href="{{ route('aluno.create') }}" class="btn btn-create float-right">
                <i class="fas fa-plus"></i> Alunos
              </a>
            </div>
          </div>
        </div>
        {{-- End --}}
        {{-- Body Card --}}
        <div class="card-body">
          <div class="table-responsive">
            {{-- Table --}}
            <table id="datatable" class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">Ações</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($students as $student)
                <tr>
                  <td>{{$student->name }}</td>
                  <td>
                    <a class="btn-view" href="{{ route('aluno.show',$student->id) }}" title="Visualizar">
                      <i class="fas fa-search"></i>
                    </a>
                    <a class="btn-edit" href="{{ route('aluno.edit',$student->id) }}" title="Alterar">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a class="btn-del" data-toggle="modal" data-target="#excluir{{$student->id}}" href="#" title="Excluir">
                      <i class="fas fa-trash-alt"></i>
                    </a>
                  </td>
                </tr>
                @empty

                <tr>
                  <td colspan="5">Nenhum registro encotrado!</td>
                </tr>
                @endforelse
              </tbody>
            </table>
            {{-- End table --}}
          </div>
        </div>
        {{-- End --}}
      </div>
    </div>
  </div>
</div>

@foreach ($students as $student)
@component('extends.modal')
@slot('link')
  {{ route('aluno.destroy',$student->id) }}
@endslot
@slot('text')
  Tem certeza que deseja excluir ?
@endslot
@slot('id')
  {{$student->id}}
@endslot
@endcomponent
@endforeach



@endsection
