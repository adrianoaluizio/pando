@extends ('layouts.theme')
@section('content')
{{Breadcrumbs::render('student-show',$student)}}
<div class="container-fluid">
  {{-- Details --}}
  <div class="row">
    <div class="col-md-12">
      <div class="card mb-3">
        <div class="card-header">
          <div class="row">
          <div class="col-md-8">
            <h1 class="title"><i class="fas fa-users"></i> {{$student->name}}</h1>
          </div>
          <div class="col-md-4">
            <a class="btn-del float-right" data-toggle="modal" data-target="#excluir{{$student->id}}" href="#" title="Excluir">
                <i class="fas fa-trash"></i>
              </a>
            <a class="btn-edit float-right mr-1" href="{{ route('aluno.edit',$student->id) }}" title="Alterar">
              <i class="fas fa-edit"></i>
            </a>
            <a href="{{ route('aluno.index') }}" class="btn-dash float-right mr-1" title="Voltar">
              <i class="fas fa-arrow-left"></i>
            </a>
          </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">

            <div class="col-md-12">
              <div class="line-chart">
                <small>Nome</small>
                <p>{{$student->name }}</p>
              </div>
            </div>

            <div class="col-md-12 mt-3">
              <h1 class="title">Cursos Inscritos</h1>
            </div>

            @foreach ($student->courses as $course)
            <div class="col-md-4">
              <div class="line-chart">
                <p>{{$course->name }}</p>
                <small>{{$course->time }}</small>
                <a href="{{ route('certificado',[$student->id,$course->id]) }}" target="_blank">Certificado</a>
              </div>
            </div>
            @endforeach

          </div>

        </div>
      </div>
    </div>
  </div>
  {{-- End Details --}}


  {{-- Card Services --}}

</div>
@component('extends.modal')
  @slot('link')
    {{ route('aluno.destroy',$student->id) }}
  @endslot
  @slot('text')
    Tem certeza que deseja excluir ?
  @endslot
  @slot('id')
    {{$student->id}}
  @endslot
@endcomponent

@endsection
