@extends('layouts.theme')
@section('content')
{{Breadcrumbs::render('student-edit',$student)}}
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <form method="POST" action="{{route ('aluno.update',$student->id)}}">
        @csrf
        @method('PATCH')
        <div class="card">
          <div class="card-header">
            <h1 class="title">{{__('Alterar Aluno')}}</h1>
          </div>
          <div class="card-body">
            <div class="form-row" id="form">
              <div class="form-group col-md-12">
                {!!Form::label('name', 'Nome:')!!}
                {!!Form::text('name', $student->name,['class'=>'form-control'])!!}
              </div>
              <div class="form-group col-md-12">
                <h1 class="title">Cursos Inscritos</h1>
              </div>

              @foreach($courses as $course)
              <div class="form-group col-md-4">
                <label>
                  <input class="" type="checkbox" name="course[]"
                  @foreach ($student->courses as $ver)
                  {{$course->verification($ver->id)}}
                  @endforeach
                  value="{{$course->id}}">
                  {{$course->name}}
                </label>
              </div>
              @endforeach

            </div>
          </div>
          <div class="card-footer">
            <button type="submit" class="btn btn-dash float-right">Alterar</button>
            <a href="{{ route('aluno.index') }}" class="btn btn-dash float-left">
              <i class="fas fa-arrow-left"></i>
            </a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
