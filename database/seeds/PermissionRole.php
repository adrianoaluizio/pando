<?php

use Illuminate\Database\Seeder;

class PermissionRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$permission_role = [
			  ['id' => '104','permission_id' => '1','role_id' => '2'],
			  ['id' => '105','permission_id' => '2','role_id' => '2'],
			  ['id' => '106','permission_id' => '3','role_id' => '2'],
			];
			DB::table('permission_role')->insert($permission_role);
    }
}
