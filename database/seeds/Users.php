<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Adriano Aluizio',
            'email' => 'adrianoaluizio@icloud.com',
            'password' => Hash::make('123456'),
        ]);
    }
}
