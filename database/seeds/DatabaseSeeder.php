<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Users::class);
        $this->call(Permission::class);
        $this->call(Role::class);
        $this->call(PermissionRole::class);
        $this->call(RoleUser::class);
    }
}
