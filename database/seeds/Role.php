<?php

use Illuminate\Database\Seeder;

class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$roles = [
			  ['id' => '1','name' => 'Administrador','label' => 'Administrador','created_at' => NULL,'updated_at' => NULL],
			  ['id' => '2','name' => 'Gerente','label' => 'Gerente do sistema','created_at' => NULL,'updated_at' => NULL],
			  ['id' => '3','name' => 'Basico','label' => 'Usuário Básico','created_at' => NULL,'updated_at' => NULL]
			];
			DB::table('roles')->insert($roles);
    }
}
