<?php

use Illuminate\Database\Seeder;

class RoleUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = [
            ['id' => '1','role_id' => '1','user_id' => '1'],
          ];
          DB::table('role_user')->insert($role_user);
    }
}
