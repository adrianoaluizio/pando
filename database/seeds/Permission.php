<?php

use Illuminate\Database\Seeder;

class Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$permissions =[
			  ['name' => 'view_user','label' => 'Visualizar usuario','type' => 'Usuario'],
			  ['name' => 'reg_user','label' => 'Cadastrar usuario','type' => 'Usuario'],
			  ['name' => 'alt_user','label' => 'Alterar usuario','type' => 'Usuario'],
			  ['name' => 'del_user','label' => 'Delete usuario','type' => 'Usuario'],

			];
			DB::table('permissions')->insert($permissions);
    }
}
