/**
 * Similar to the Date (dd/mm/YY) data sorting plug-in, this plug-in offers 
 * additional  flexibility with support for spaces between the values and
 * either . or / notation for the separators.
 *
 * Please note that this plug-in is **deprecated*. The
 * [datetime](//datatables.net/blog/2014-12-18) plug-in provides enhanced
 * functionality and flexibility.
 *
 *  @name Date (dd . mm[ . YYYY]) 
 *  @summary Sort dates in the format `dd/mm/YY[YY]` (with optional spaces)
 *  @author [Robert Sedovšek](http://galjot.si/)
 *  @deprecated
 *
 *  @example
 *    $('#example').dataTable( {
 *       columnDefs: [
 *         { type: 'date-eu', targets: 0 }
 *       ]
 *    } );
 */

function formatPrice(value) {
  let val = (value/1).toFixed(2).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}


jQuery.extend(jQuery.fn.dataTableExt.oSort, {
  "date-eu-pre": function(date) {
    date = date.replace(" ", "");

    if (!date) {
      return 0;
    }

    var year;
    var eu_date = date.split(/[\.\-\/]/);

    /*year (optional)*/
    if (eu_date[2]) {
      year = eu_date[2];
    } else {
      year = 0;
    }

    /*month*/
    var month = eu_date[1];
    if (month.length == 1) {
      month = 0 + month;
    }

    /*day*/
    var day = eu_date[0];
    if (day.length == 1) {
      day = 0 + day;
    }

    return (year + month + day) * 1;
  },

  "date-eu-asc": function(a, b) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
  },

  "date-eu-desc": function(a, b) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
  }
});



$(document).ready(function() {
  $('#sidebarCollapse').on('click', function() {
    $('#sidebar').toggleClass('active');
  });
});

// Tooltips Initialization
$(function() {
  $('[data-toggle="tooltip"]').tooltip()
})



// MASCARA
$(document).ready(function() {
  $(".cnpj").mask("99.999.999/9999-99");
  $(".cep").mask("99999-999");
  $(".cpf").mask("999.999.999-99");
  $('.money').mask("#.##0,00" , { reverse:true});
});


// Select 2

$(document).ready(function() {
  $('.simple-select2-mult').select2({
    containerCssClass: ':all:',
    placeholder: "Selecione as opções",
    allowClear: true
  });

  $('.simple-select2-sm').select2({
    containerCssClass: ':all:',
    placeholder: "Selecione as opções:",
    allowClear: true,
  });
});

// Data Table
$(document).ready(function() {
  $('#datatable').DataTable({
    retrieve: true,
    "pageLength": 100,
    "lengthMenu": [100, 250, 500],
    language: {
      sEmptyTable: "Nenhum registro encontrado",
      sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
      sInfoFiltered: "(Filtrados de _MAX_ registros)",
      sInfoPostFix: "",
      sInfoThousands: ".",
      sLengthMenu: "_MENU_ Resultados por página",
      sLoadingRecords: "Carregando...",
      sProcessing: "Processando...",
      sZeroRecords: "Nenhum registro encontrado",
      sSearch: "Pesquisar",
      sNext: "<i class='fas fa-angle-right'></i>",
      sPrevious: "<i class='fas fa-angle-left'></i>",
      sFirst: "<i class='fas fa-fast-backward'></i>",
      sLast: "<i class='fas fa-fast-forward'></i>",
      sSortAscending: ": Ordenar colunas de forma ascendente",
      sSortDescending: ": Ordenar colunas de forma descendente",
      searchPlaceholder: "Pesquisar",
      paginate: {
        "previous": "Anterior",
        "next": "Próxima"
      },
    }
  });


  $('#datatableData').DataTable({
    retrieve: true,
    "pageLength": 100,
    "lengthMenu": [100, 250, 500],
     "order": [[ 0, "desc" ]],
    columnDefs: [{
      type: 'date-eu',
      targets: 0
    }],
    language: {
      sEmptyTable: "Nenhum registro encontrado",
      sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
      sInfoFiltered: "(Filtrados de _MAX_ registros)",
      sInfoPostFix: "",
      sInfoThousands: ".",
      sLengthMenu: "_MENU_ Resultados por página",
      sLoadingRecords: "Carregando...",
      sProcessing: "Processando...",
      sZeroRecords: "Nenhum registro encontrado",
      sSearch: "Pesquisar",
      sNext: "<i class='fas fa-angle-right'></i>",
      sPrevious: "<i class='fas fa-angle-left'></i>",
      sFirst: "<i class='fas fa-fast-backward'></i>",
      sLast: "<i class='fas fa-fast-forward'></i>",
      sSortAscending: ": Ordenar colunas de forma ascendente",
      sSortDescending: ": Ordenar colunas de forma descendente",
      searchPlaceholder: "Pesquisar",
      paginate: {
        "previous": "Anterior",
        "next": "Próxima"
      },
    }
  });



  $('#datatablesum').DataTable( {
    retrieve: true,
    "pageLength": 100,
    "lengthMenu": [100, 250, 500],
    language: {
      sEmptyTable: "Nenhum registro encontrado",
      sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
      sInfoFiltered: "(Filtrados de _MAX_ registros)",
      sInfoPostFix: "",
      sInfoThousands: ".",
      sLengthMenu: "_MENU_ Resultados por página",
      sLoadingRecords: "Carregando...",
      sProcessing: "Processando...",
      sZeroRecords: "Nenhum registro encontrado",
      sSearch: "Pesquisar",
      sNext: "<i class='fas fa-angle-right'></i>",
      sPrevious: "<i class='fas fa-angle-left'></i>",
      sFirst: "<i class='fas fa-fast-backward'></i>",
      sLast: "<i class='fas fa-fast-forward'></i>",
      sSortAscending: ": Ordenar colunas de forma ascendente",
      sSortDescending: ": Ordenar colunas de forma descendente",
      searchPlaceholder: "Pesquisar",
      paginate: {
        "previous": "Anterior",
        "next": "Próxima"
      },
    },
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\R$,]/g, '')*1 :
        typeof i === 'number' ?
      i : 0;
      };

      // Total over all pages
      total = api
      .column( 4 )
      .data()
      .reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );

      // Total over this page
      pageTotal = api
      .column( 4, { page: 'current'} )
      .data()
      .reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );

      // Update footer
      $( api.column( 4 ).footer() ).html('R$ '+ formatPrice(total));
    }
  } );





});