<?php

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

	Route::get('/', 'HomeController@index')->name('dashboard');
	Route::get('/logout', 'HomeController@getLogout')->name('logout');
    Route::resource('user', 'UserController');
    Route::resource('aluno', 'Pando\StudentController');
    Route::resource('curso', 'Pando\CourseController');
	Route::get('certificado/{student}/{course}', 'Pando\StudentController@pdf')->name('certificado');

});
