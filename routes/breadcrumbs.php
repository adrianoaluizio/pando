<?php

// Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Painel de Controle', route('dashboard'));
});

// Dashboard > Student
Breadcrumbs::for('student', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Aluno', route('aluno.index'));
});

Breadcrumbs::for('student-create', function ($trail) {
    $trail->parent('student');
    $trail->push('Cadastrar', route('aluno.create'));
});

Breadcrumbs::for('student-show', function ($trail,$student) {
    $trail->parent('student');
    $trail->push($student->name, route('aluno.show',$student->id));
});

Breadcrumbs::for('student-edit', function ($trail,$student) {
    $trail->parent('dashboard');
    $trail->push('Editar', route('aluno.edit',$student->id));
});


// Dashboard > Course
Breadcrumbs::for('course', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Curso', route('curso.index'));
});

Breadcrumbs::for('course-create', function ($trail) {
    $trail->parent('course');
    $trail->push('Cadastrar', route('curso.create'));
});

Breadcrumbs::for('course-show', function ($trail,$course) {
    $trail->parent('course');
    $trail->push($course->name, route('curso.show',$course->id));
});

Breadcrumbs::for('course-edit', function ($trail,$course) {
    $trail->parent('dashboard');
    $trail->push('Editar', route('curso.edit',$course->id));
});

